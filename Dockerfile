FROM python:3.7-alpine
EXPOSE 80

RUN mkdir /vampi
RUN apk --update add bash nano

ENV vulnerable=1
ENV tokentimetolive=300

COPY . /vampi
WORKDIR /vampi

# Installing Dependencies
RUN apk add gcc musl-dev python3-dev && \
  apk add --update g++ && \
  pip install -r requirements.txt && \
  apk del gcc musl-dev python3-dev g++ && \
  rm -rf /var/cache/apk/*

RUN pip install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["app.py"]
