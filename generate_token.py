import requests
import json
RegisterPayload = {
    "username": "Test",
    "password": "TestP@ssw0rd",
    "email": "Test@gmail.com"
}

LoginPayload = {
    "username": "Test",
    "password": "TestP@ssw0rd"
}

RegisterResp = requests.post("http://vulnapi:80/users/v1/register", json=RegisterPayload)
LoginResp = requests.post("http://vulnapi:80/users/v1/login", json=LoginPayload)
JsonResponse = LoginResp.json()
data = dict()
token = dict()
token["Authorization"] = "Bearer " + JsonResponse["auth_token"]
data["headers"] = token
with open('token.json', 'w') as outfile:
    json.dump(data, outfile)
